﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace ej5
{
    interface Entregable
    {
        void entregar();
        void devolver();
        bool isEntregado();
        Entregable compareTo(Entregable o);
    }

    class Serie : Entregable
    {
        private string titulo = "";
        private int numero_temporadas = 3;
        private string genero = "";
        private string creador = "";
        private bool prestado = false;
        public Serie() { }
        public Serie(string titulo, string creador)
        {
            this.titulo = titulo;
            this.creador = creador;
        }
        public Serie(string titulo, string genero, string creador, int temporadas)
        {
            this.titulo = titulo;
            this.genero = genero;
            this.creador = creador;
            this.numero_temporadas = temporadas;
        }
        public string getTitulo
        {
            get
            {
                return this.titulo;
            }
        }
        public string getGenero
        {
            get
            {
                return this.genero;
            }
        }
        public string getCreador
        {
            get
            {
                return this.creador;
            }
        }
        public int getNumeroTemporadas
        {
            get
            {
                return this.numero_temporadas;
            }
        }
        public string setTitulo
        {
            set
            {
                this.titulo = value;
            }
        }
        public string setCreador
        {
            set
            {
                this.creador = value;
            }
        }
        public string setGenero
        {
            set
            {
                this.genero = value;
            }
        }
        public int setNumeroTemporadas
        {
            set
            {
                this.numero_temporadas = value;
            }
        }
        public void devolver()
        {
            this.prestado = false;
        }

        public void entregar()
        {
            this.prestado = true;
        }

        public bool isEntregado()
        {
            return this.prestado;
        }

        public Entregable compareTo(Entregable o)
        {
            if (o is Serie)
            {
                Serie z = (Serie)o;
                if (z.getNumeroTemporadas > this.numero_temporadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if(o is Videojuego)
            {
                Videojuego z = (Videojuego)o;
                if(z.getHorasEstimadas > this.numero_temporadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }
    }
    class Videojuego : Entregable
    {
        private string titulo = "";
        private string genero = "";
        private string compañia = "";
        private int horas_estimadas = 10;
        private bool prestado = false;

        public Videojuego() { }

        public Videojuego(string titulo, int horas_estimadas)
        {
            this.titulo = titulo;
            this.horas_estimadas = horas_estimadas;
        }
        public Videojuego(string titulo, string genero, string compañia, int horas_estimadas)
        {
            this.titulo = titulo;
            this.genero = genero;
            this.horas_estimadas = horas_estimadas;
            this.compañia = compañia;
        }

        public string getTitulo
        {
            get
            {
                return this.titulo;
            }
        }
        public string getGenero
        {
            get
            {
                return this.genero;
            }
        }
        public string getCompañia
        {
            get
            {
                return this.compañia;
            }
        }
        public int getHorasEstimadas
        {
            get
            {
                return this.horas_estimadas;
            }
        }
        public string setTitulo
        {
            set
            {
                this.titulo = value;
            }
        }
        public string setCompañia
        {
            set
            {
                this.compañia = value;
            }
        }
        public string setGenero
        {
            set
            {
                this.genero = value;
            }
        }
        public int setHorasEstimadas
        {
            set
            {
                this.horas_estimadas = value;
            }
        }


        public void devolver()
        {
            this.prestado = false;
        }

        public void entregar()
        {
            this.prestado = true;
        }

        public bool isEntregado()
        {
            return this.prestado;
        }
        public Entregable compareTo(Entregable o)
        {
            if(o is Videojuego)
            {
                Videojuego z = (Videojuego)o;
                if(z.getHorasEstimadas > this.horas_estimadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }else if(o is Serie)
            {
                Serie z = (Serie)o;
                if (z.getNumeroTemporadas > this.horas_estimadas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Serie[] series = new Serie[5];
            Videojuego[] videojuegos = new Videojuego[5];
            series[0] = new Serie("lala", "pipo");
            series[1] = new Serie("lalala", "extraterrestre", "alien", 123);
            series[2] = new Serie("100", "z");
            series[3] = new Serie("A51", "BillGates");
            series[4] = new Serie("Pinocho", "Geppetto");
            videojuegos[0] = new Videojuego("Minecraft",10000);
            videojuegos[1] = new Videojuego("CSGO", 0);
            videojuegos[2] = new Videojuego("GTAV", 200);
            videojuegos[3] = new Videojuego("tetris", "para para viejos", "algunchinitoseguro", 10);
            videojuegos[4] = new Videojuego("Fallout", 50);
            series[4].entregar();
            videojuegos[2].entregar();
            videojuegos[1].entregar();
            series[3].entregar();
            series[0].entregar();
            int juegos_prestados = 0, series_prestadas = 0;
            Serie mas_larga = series[0];
            Videojuego mas_largo = videojuegos[0];
            for(int w = 0; w <5; w++)
            {
                if (series[w].isEntregado())
                {
                    series_prestadas++;
                    series[w].devolver();
                }
                if (videojuegos[w].isEntregado())
                {
                    juegos_prestados++;
                    videojuegos[w].devolver();
                }
                if (series[w].getNumeroTemporadas > mas_larga.getNumeroTemporadas)
                {
                    mas_larga = series[w];
                }
                if(videojuegos[w].getHorasEstimadas > mas_largo.getHorasEstimadas)
                {
                    mas_largo = videojuegos[w];
                }
            }
            Console.WriteLine("Habia {0} videojuegos prestados y {1} series prestadas.", juegos_prestados, series_prestadas);
            Console.WriteLine("La serie mas larga es: {0}", mas_larga.getTitulo);
            Console.WriteLine("El juego con mas horas estimadas es {0}", mas_largo.getTitulo);
        }
    }
}

