﻿using System;
using System.Reflection.Metadata.Ecma335;

namespace ej3
{
    class Password
    {
        private int longitud = 8;
        private string contraseña;

        public Password() {
            generarPassword();
        }

        public Password(int longitud)
        {
            this.longitud = longitud;
            generarPassword();
        }
        public string Contraseña
        {
            get
            {
                return this.contraseña;
            }
        }
        public int Longitud
        {
            get
            {
                return this.longitud;
            }
            set
            {
                this.longitud = value;
            }
        }
        
        public void generarPassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[this.longitud];
            var random = new Random(DateTime.UtcNow.Millisecond);

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            this.contraseña = new String(stringChars);
        }
        public bool esFuerte()
        {
            int cant_mayus = 0;
            int cant_mins = 0;
            int cant_nums = 0;
            foreach(char c in this.contraseña)
            {
                if (this.esMayus(c))
                {
                    cant_mayus++;
                }else if (this.esMinus(c))
                {
                    cant_mins++;
                }
                else if(this.esNum(c))
                {
                    cant_nums++;
                }
            }
            if(cant_mayus>2 && cant_mins>1 && cant_nums > 5)
            {
                return true;
            }
            return false;
        }
        private bool esMayus(char c)
        {
            string mayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            bool esMayus = false;
            foreach(char z in mayus)
            {
                if(c == z)
                {
                    esMayus = true;
                }
            }
            return esMayus;
        }
        private bool esMinus(char c)
        {
            string minus = "abcdefghijklmnopqrstuvwxyz";
            bool esMinus = false;
            foreach (char z in minus)
            {
                if (c == z)
                {
                    esMinus = true;
                }
            }
            return esMinus;
        }
        private bool esNum(char c)
        {
            string nums = "0123456789";
            bool esNum = false;
            foreach (char z in nums)
            {
                if (c == z)
                {
                    esNum = true;
                }
            }
            return esNum;
        }
    }

    public class Ejecutable
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese la cantidad de contraseñas a generar");
            int cant = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese la longitud de las contraseñas por teclado");
            int longitud = int.Parse(Console.ReadLine());
            Password[] contraseñas = new Password[cant];
            bool[] es_fuerte = new bool[cant];
            for (int i = 0; i < cant; i++)
            {
                contraseñas[i] = new Password(longitud);
                es_fuerte[i] = contraseñas[i].esFuerte();
                Console.WriteLine("{0} {1}", contraseñas[i].Contraseña, es_fuerte[i]);
            }
        }

    }
}
