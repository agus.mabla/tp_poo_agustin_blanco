﻿using System;

namespace ej6
{
    class Libro
    {
        private int ISBN;
        private string titulo;
        private string autor;
        private int numero_paginas;

        public Libro(string autor, string titulo, int ISBN, int numero_paginas)
        {
            this.titulo = titulo;
            this.autor = autor;
            this.ISBN = ISBN;
            this.numero_paginas = numero_paginas;
        }
        
        public int getNumeroPaginas
        {
            get
            {
                return this.numero_paginas;
            }
        }
        public int getISBN
        {
            get
            {
                return this.ISBN;
            }
        }
        public string toString()
        {
            string temp = "El libro " + this.ISBN + " creado por " + this.autor + " tiene " + this.numero_paginas + " paginas.";
            return temp;
        }

    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Libro[] libros = new Libro[2];
            libros[0] = new Libro("JKR", "Harry Potter", 123456, 500);
            libros[1] = new Libro("El principito", "Antoine", 321654, 80);
            Libro mas_paginas = libros[0];
            for(int z =0; z <libros.Length; z++)
            {
                Console.WriteLine(libros[z].toString());
                if(libros[z].getNumeroPaginas > mas_paginas.getNumeroPaginas)
                {
                    mas_paginas = libros[z];                }
            }
            Console.WriteLine("El libro con mas paginas es {0}", mas_paginas.getISBN);
        }
    }
}
